$(document).ready(function() {

    $('div.section').on('mouseover', function() {
        this.id = "focused";
    });

    $('div.section').on('mouseout', function() {
        this.id = "";
    });

    $('li').on('mouseover', function() {
        this.id = "hover";
    });

    $('li').on('mouseout', function() {
        this.id = "";
    });
});